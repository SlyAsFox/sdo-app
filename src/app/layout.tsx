"use client"
import "./globals.css";
import { SnackbarProvider } from 'notistack';
import { ReactNode } from 'react'
import queryClient from "@/shared/api/query-client";
import { QueryClientProvider } from '@tanstack/react-query';
import { Header } from "@/widgets/header/ui";

interface RootLayoutProps {
    children: ReactNode;
    title: string;
}

function RootLayout( { children, title = 'SDO App' }: RootLayoutProps ) {
    return (
        <html lang="uk">
            <head>
                <title>{title}</title>
                <meta charSet="utf-8"/>
                <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
            </head>
            <body className="layout">
                <QueryClientProvider client={queryClient}>
                    <SnackbarProvider maxSnack={3}>
                        <Header className="layout__header"/>
                        <main className="layout__main">
                                {children}
                        </main>
                    </SnackbarProvider>
                </QueryClientProvider>
            </body>
        </html>
    );
}

export default RootLayout;
