import { TechnologiesList } from "@/widgets/technologies-list/ui";

export const TechnologiesPage = () => {
    return (
        <TechnologiesList/>
    );
};

export default TechnologiesPage;
