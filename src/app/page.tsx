import Image from "next/image";
import Link from 'next/link';

export default function Home() {
    return (
        <section className="home">
            <div className="home__logo-container">
                <Image
                    src="/competence.png"
                    width={200}
                    height={200}
                    alt="Logo"
                    className="home__logo-container__logo-image"
                />
                <h1 className="home__logo-container__heading">My skills</h1>
                <p className="home__logo-container__description">
                    Платформа, створена для ефективного представлення та просування ваших професійних навичок у цифровому світі.
                </p>
            </div>

            <div className="home__buttons">
                <Link href="/start" className="btn-primary">Почати</Link>
                <Link href="/learn-more" className="btn-secondary">Дізнатися більше →</Link>
            </div>

            <div className="home__donation">
                <p>🇺🇦</p>
                <p className="home__donation-text">Зробити внесок на рахунок фонду допомоги ЗСУ</p>
                <Link
                    href="https://prytulafoundation.org/donation"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="home__donation-link"
                >
                    <p>Підтримати</p>
                </Link>
            </div>
        </section>
    );
}
