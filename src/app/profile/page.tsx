"use client"
import { useRouter } from "next/navigation";
import ErrorBoundary from "@/shared/ui/ErrorBoundary";
import { Loader } from "@/shared/ui";
import ErrorFallback from "@/shared/ui/ErrorFallback";
import { useEffect, useState, Suspense } from "react";
import { UserById } from "@/widgets/user-by-id";

const ProfilePage = () => {
    const router = useRouter();
    const [ id, setId ] = useState<string | null>( null );

    useEffect( () => {
        try {
            const user = JSON.parse( localStorage.getItem( 'user' ) );
            if ( user && user.id ) {
                setId( user.id );
            } else {
                router.push( '/login' );
            }
        } catch ( e ) {
            router.push( '/login' );
        }
    }, [] );

    if ( !id ) {
        return <Loader />;
    }

    return (
        <ErrorBoundary fallback={<ErrorFallback onRetry={() => window.location.reload()} />}>
            <Suspense fallback={<Loader/>}>
                <UserById id={id} />
            </Suspense>
       </ErrorBoundary>
    );
}

export default ProfilePage;
