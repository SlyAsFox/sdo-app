import { EditProfile } from "@/widgets/update-profile";

export default function EditProfilePage() {
    return (
        <section className="edit-profile">
            <EditProfile />
        </section>
  )
}

