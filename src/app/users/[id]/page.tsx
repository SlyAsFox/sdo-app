'use client'
import { useParams } from "next/navigation";
import { Suspense } from "react";
import { Loader } from "@/shared/ui";
import ErrorBoundary from "@/shared/ui/ErrorBoundary";
import ErrorFallback from "@/shared/ui/ErrorFallback";
import { UserById } from "@/widgets/user-by-id";

const UserByIdPage = () => {
    const { id } = useParams();

    return (
        <ErrorBoundary fallback={<ErrorFallback onRetry={() => window.location.reload()} />}>
            <Suspense fallback={( <Loader/> )}>
                <UserById id={id}/>
            </Suspense>
        </ErrorBoundary>
    );
}

export default UserByIdPage;
