import { UsersList } from "@/widgets/users-list";


const UsersPage = () => {
    return (
        <section className="users-page">
            <UsersList/>
        </section>
    );
}

export default UsersPage;
