import { FC } from "react";
import Image from "next/image";
import { FeatureList } from "@/widgets/features-list";

const AboutPage: FC = () => {
    return (
        <section className="about">
            <div className="about__header">
                <Image
                    src="/competence.png"
                    width={80}
                    height={80}
                    alt="Logo"
                    className="about__logo"

                />
                <h1 className="about__title">My skills</h1>
            </div>

            <div className="about__intro">
                <p className="about__subtitle">{"Твоя експертиза та твої навички, то твоя сила! А інші люди тому доказ."}</p>

                <p className="about__description">
                    {`"My skills" - це інноваційна платформа, створена для ефективного представлення та просування ваших професійних
                    навичок у цифровому світі. Наші зручні користувацькі профілі призначені допомогти вам виділити свій досвід та
                    експертизу в різних сферах. А допоможуть нам у цьому ваші колеги.`}
                </p>
            </div>

            <FeatureList/>
        </section>
    );
};

export default AboutPage;
