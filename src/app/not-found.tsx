import Link from 'next/link';
import { FC } from "react";

const NotFoundPage: FC = () => {
    return (
        <div className="not-found">
            <section className="not-found__section">
                <h1 className="not-found__error-code">404</h1>
                <h2 className="not-found__title">Чогось не вистачає</h2>
                <p className="not-found__subtitle">
                    Вибачте, ми не можемо знайти цю сторінку. Але на домашній сторінці ви знайдете багато цікавого.
                </p>
                <Link href="/" className="btn-primary">
                        Назад на головну сторінку
                </Link>
            </section>
        </div>
    );
};

export default NotFoundPage;
