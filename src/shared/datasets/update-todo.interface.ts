export interface IUpdateTodo {
    text?: string;
    isCompleted?: boolean;
}
