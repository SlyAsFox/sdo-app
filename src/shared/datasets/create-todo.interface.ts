export interface ICreateTodo {
    text: string;
    isCompleted: boolean;
}
