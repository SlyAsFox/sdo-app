import axios, { AxiosInstance, AxiosRequestConfig, InternalAxiosRequestConfig, AxiosResponse } from 'axios';
import { ICreateTodo, IUpdateTodo } from "@/shared/datasets";
import { UserEntity } from "@/entities/user";

const instance: AxiosInstance = axios.create( {
    baseURL: 'http://localhost:3002/api/v1/',
    headers: { 'Content-Type': 'application/json' }
} );

// Інтерцептор для додавання токена до кожного запиту
instance.interceptors.request.use(
    ( config: InternalAxiosRequestConfig ): InternalAxiosRequestConfig => {
        const token = localStorage.getItem( 'accessToken' );
        if ( token ) {
            if ( !config.headers ) {
                config.headers = {};
            }
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    ( error ) => {
        return Promise.reject( error );
    }
);

// Інтерцептор для обробки 401 помилок
instance.interceptors.response.use(
    ( response: AxiosResponse ) => response,
    async ( error ) => {
        const originalRequest = error.config;
        if ( error.response && error.response.status === 401 && !originalRequest._retry ) {
            originalRequest._retry = true;
            const success = await refreshAccessToken();
            if ( success ) {
                originalRequest.headers['Authorization'] = `Bearer ${localStorage.getItem( 'accessToken' )}`;
                return instance( originalRequest );
            }
        }
        if ( error.response ) {
            throw new Error( error.response.data.message )
        }
        throw error;
    }
);

interface ApiListResponse<T> {
    data: T[];
    count: number;
}

const handleResponse = <T>( promise: Promise<any> ): Promise<ApiListResponse<T> | T | void> => {
    return promise
        .then( response => {
            if ( response.data && typeof response.data === 'object' && 'count' in response ) {
                return {
                    data: response.data,
                    count: response.count,
                } as ApiListResponse<T>;
            }
            return response.data;
        } )
        .catch( error => {
            console.error( 'API call error:', error );
            throw error;
        } );
};

const api = {
    getUsers: ( params?: Record<string, any> ): Promise<ApiListResponse<UserEntity>> =>
        handleResponse<UserEntity>( instance.get( 'users', { params } ) ),
    getUserById: ( userId: string, params?: Record<string, any> ): Promise<UserEntity> =>
        handleResponse<UserEntity>( instance.get( `users/${userId}`, { params } ) ),
    getUserTodos: ( userId: string ): Promise<any> =>
        handleResponse( instance.get( `users/${userId}/todos` ) ),
    addTodo: ( data: ICreateTodo ): Promise<any> =>
        handleResponse( instance.post( `todos`, data ) ),
    updateTodo: ( id: string, data: IUpdateTodo ): Promise<any> =>
        handleResponse( instance.patch( `todos/${id}`, data ) ),
    deleteTodo: ( id: string ): Promise<any> =>
        handleResponse( instance.delete( `todos/${id}` ) ),
    getProfile: async (): Promise<UserEntity> =>{
        const data = await handleResponse<UserEntity>( instance.get( 'users/profile' ) )
        localStorage.setItem( 'user', JSON.stringify( data ) );
        return data as UserEntity;
    },
    updateProfile: async ( data: UserEntity ): Promise<UserEntity> =>{
        const updatedProfile = await handleResponse<UserEntity>( instance.patch( 'users/profile', data ) )
        localStorage.setItem( 'user', JSON.stringify( updatedProfile ) );
        return updatedProfile as UserEntity;
    },
    login: async ( email: string, password: string ): Promise<any> => {
        const response = await instance.post( 'auth/user/login', { email, password } )
        const { accessToken, refreshToken, user } = response.data;
        localStorage.setItem( 'accessToken', accessToken );
        localStorage.setItem( 'refreshToken', refreshToken );
        localStorage.setItem( 'user', JSON.stringify( user ) );
        return response.data;
    },
    signup: async ( data: { name: string, email: string, password: string } ): Promise<any> => {
        const response = await instance.post( 'auth/user/signup', data );
        const { accessToken, refreshToken, user } = response.data;
        localStorage.setItem( 'accessToken', accessToken );
        localStorage.setItem( 'refreshToken', refreshToken );
        localStorage.setItem( 'user', JSON.stringify( user ) );
        return response.data;
    },
    uploadFile: ( formData: FormData ): Promise<any> => {
        return handleResponse( instance.post( 'files/upload', formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        } ) );
    },
};

const refreshAccessToken = async (): Promise<boolean> => {
    const refreshToken = localStorage.getItem( 'refreshToken' );
    if ( !refreshToken ) {
        return false;
    }

    try {
        const response = await instance.post( 'auth/user/refresh', { refreshToken } );
        const { accessToken, refreshToken: newRefreshToken } = response.data;

        localStorage.setItem( 'accessToken', accessToken );
        localStorage.setItem( 'refreshToken', newRefreshToken );

        return true;
    } catch ( error ) {
        console.error( 'Failed to refresh token:', error );
        localStorage.removeItem( 'accessToken' );
        localStorage.removeItem( 'refreshToken' );
        localStorage.removeItem( 'user' );
        return false;
    }
};


export default api;
