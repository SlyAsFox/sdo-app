import { ChangeEvent, FC, useEffect, useState } from 'react';
import { useSnackbar } from 'notistack';
import { SnackbarTypesEnum } from "@/shared/datasets";
import imageCompression from 'browser-image-compression';
import mime from "mime-types";
import { AiOutlineCloudUpload } from "react-icons/ai";
import api from "@/shared/api";

const compressImage = async ( file: File, options ): Promise<File> => {
    try {
        const originalExtension = file.name.split( '.' ).pop();
        const mimeType = mime.lookup( file.name ) || 'application/octet-stream';

        const compressedFile = await imageCompression( file, {
            ...options,
            fileType: mimeType,
        } );

        // Створення нового файлу з оригінальним розширенням
        return new File(
            [ compressedFile ],
            `${compressedFile.name.split( '.' )[0]}.${originalExtension}`,
            {
                type: compressedFile.type,
                lastModified: compressedFile.lastModified,
            }
        );
    } catch ( error ) {
        console.error( 'Error compressing file:', error );
        return file;
    }
};

const previewCompressionOptions = {
    maxSizeMB: 0.1,
    maxWidthOrHeight: 1920,
    useWebWorker: true,
}
const originalCompressionOptions = {
    maxSizeMB: 1,
    maxWidthOrHeight: 1920,
    useWebWorker: true,
}

interface UploadPhotoProps {
    handleAvatarChange: ( preview: string, original: string ) => void;
    originalUrl?: string;
    previewUrl?: string;
}

export const UploadPhoto: FC<UploadPhotoProps> = ( { handleAvatarChange, originalUrl = null, previewUrl = null } ) => {
    const [ original, setOriginal ] = useState<string | null>( originalUrl || null );
    const [ , setPreview ] = useState<string | null>( previewUrl || null );
    const { enqueueSnackbar } = useSnackbar();
    const MB = 1024 * 1024;

    useEffect( () => {
        if( originalUrl ) {
            setOriginal( originalUrl );
        }
    }, [ originalUrl ] );

    const handleFileChange = async ( e: ChangeEvent<HTMLInputElement> ) => {
        if ( e.target.files && e.target.files.length ) {
            try {
                let file = e.target.files[0];

                if ( file.size > MB ) {
                    file = await compressImage( file, originalCompressionOptions );
                }
                const previewFile = await compressImage( file, previewCompressionOptions );

                await handleUpload( file, previewFile );

                enqueueSnackbar( 'File successfully uploaded.', { variant: SnackbarTypesEnum.SUCCESS } );
            } catch ( e ) {
                enqueueSnackbar( e.message, { variant: SnackbarTypesEnum.ERROR } );
            }
        }
    };

    const handleUpload = async ( originalFile: File, previewFile: File ) => {
        const formData = new FormData();
        formData.append( 'original', originalFile );
        formData.append( 'preview', previewFile );

        try {

            const response = await api.uploadFile( formData );
            const { original, preview } = response;
            setOriginal( original );
            setPreview( preview );

            handleAvatarChange( preview, original );
        } catch ( error ) {
            console.error( 'Error uploading file:', error );
        }
    };

    return (
        <div className="upload-photo-container">
            <label htmlFor="dropzone-file" className={`${ original ? 'upload-photo-label--with-image': '' } upload-photo-label`}>
                {
                    original
                        ? (
                            <img src={original} alt="Uploaded file" className="upload-photo-image" />
                        )
                        : (
                            <div className="upload-photo-placeholder">
                                <AiOutlineCloudUpload size={48}/>
                                <p className="upload-photo-text--main">
                                    <span className="upload-photo-text--bold">Click to upload</span> or drag and drop
                                </p>
                                <p className="upload-photo-text--sub">PNG, JPG (MAX. 800x400px)</p>
                            </div>
                        )
                }
                <input
                    id="dropzone-file"
                    type="file"
                    className="upload-photo-input"
                    accept=".jpg,.png"
                    onChange={handleFileChange}
                />
            </label>
        </div>
    );
};
