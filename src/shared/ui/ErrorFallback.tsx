import React from 'react';

interface ErrorFallbackProps {
    onRetry: () => void;
    message?: string
}

const ErrorFallback: React.FC<ErrorFallbackProps> = ( { onRetry, message = 'Щось пішло не так. Спробуйте ще раз.' } ) => {
    return (
        <div className="error-fallback">
            <div className="error-fallback__content">
                <p>{message}</p>
                <button onClick={onRetry} className="btn-primary">Retry</button>
            </div>
        </div>
    );
};

export default ErrorFallback;
