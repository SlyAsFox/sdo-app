import { FC, ChangeEvent, useEffect, useState } from 'react';

interface InputProps {
    name: string;
    type: string;
    placeholder?: string;
    value?: string | undefined;
    label: string;
    onChange?: ( e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement> ) => void;
    validate?: ( name: string, value: string ) => string;
    setErrors?: ( name: string, error: string ) => void;
    isDisabled?: boolean;
}


export const Input: FC<InputProps> = ( { name, type, placeholder, value, label, onChange, validate, setErrors, isDisabled = false  } ) => {
    const [ error, setError ] = useState( '' );
    const [ internalValue, setInternalValue ] = useState( value || '' );

    useEffect( () => {
        setInternalValue( value || '' );
    }, [ value ] );

    const handleChange = ( e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement> ) => {
        const { value } = e.target;
        setInternalValue( value );

        if ( validate ) {
            const errorMessage = validate( name, value );
            setError( errorMessage );
            if ( setErrors ) {
                setErrors( name, errorMessage );
            }
        }

        if ( onChange ) {
            onChange( e );
        }
    };

    return (
        <div className={`input`}>
            { label ? <label htmlFor={name} className="input__label">{label}</label> : ''}
            {type !== 'textarea' ? (
                <input
                    id={name}
                    type={type}
                    name={name}
                    placeholder={placeholder || ''}
                    value={internalValue}
                    className={`${error ? 'input__field--error' : ''} input__field`}
                    onChange={handleChange}
                    disabled={!!isDisabled}
                />
            ) : (
                <textarea
                    className={`input__textarea ${
                        error ? 'border-red-500' : 'border-gray-300'
                    }`}
                    placeholder={placeholder || ''}
                    name={name}
                    value={internalValue}
                    onChange={handleChange}
                    disabled={isDisabled}
                />
            )}
            <p className="input__message--error">{error || '\u00A0'}</p> {/* Using non-breaking space */}
        </div>
    );
};
