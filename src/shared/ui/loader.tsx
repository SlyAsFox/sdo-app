import { FC } from 'react';
import { TailSpin } from "react-loader-spinner";

export const Loader: FC = () => {
    try{
        return (
            <div className={'loader-wrapper'}>
                <TailSpin
                    visible={true}
                    height="64"
                    width="64"
                    color="#3B82F6"
                    ariaLabel="tail-spin-loading"
                    radius="1"
                    wrapperStyle={{}}
                    wrapperClass=""
                />
            </div>
        );
    } catch ( e ) {
        throw e;
    }
};
