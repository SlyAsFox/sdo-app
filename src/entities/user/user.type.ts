export type UserEntity = {
    id: string;
    name: string;
    title: string;
    description: string;
    email: string;
    password?: string;
    location: string;
    repeatPassword: string;
    avatarOriginal: string;
    avatarPreview: string;
    githubUrl?: string;
    linkedinUrl?: string;
    instagramUrl?: string;
    facebookUrl?: string;
}
