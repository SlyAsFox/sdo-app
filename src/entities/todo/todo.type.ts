export type TodoEntity = {
    id: string;
    text: string;
    userId: string;
    isCompleted: boolean;
}
