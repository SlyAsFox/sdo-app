import { RefObject, useEffect, useRef, useState } from "react";
import { usePathname, useRouter } from "next/navigation";

export function useHeader() {
    const [ user, setUser ] = useState( null );
    const [ isMenuOpen, setIsMenuOpen ] = useState( false );
    const navRef = useRef( null ) as RefObject<HTMLDivElement>
    const pathname = usePathname();
    const router = useRouter()

    const toggleMenu = () => setIsMenuOpen( !isMenuOpen );

    useEffect( () => {
        const handleClickOutside = ( event ) => {
            if ( navRef.current && !navRef.current.contains( event?.target ) ) {
                toggleMenu();
            }
        };

        document.addEventListener( 'click', handleClickOutside );
        return () => {
            document.removeEventListener( 'click', handleClickOutside );
        };
    }, [ navRef, isMenuOpen ] );

    useEffect( () => {
        const storedUser = localStorage.getItem( 'user' );
        if ( storedUser ) {
            setUser( JSON.parse( storedUser ) );
        }
    }, [ pathname ] );

    const handleLogout = () => {
        localStorage.removeItem( 'accessToken' );
        localStorage.removeItem( 'refreshToken' );
        localStorage.removeItem( 'user' );
        setUser( null );
        router.push( '/' );
    };

    return {
        handleLogout, user, pathname, isMenuOpen, toggleMenu, navRef
    }
}
