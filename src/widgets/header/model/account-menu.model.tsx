import { RefObject, useEffect, useRef, useState } from "react";
import { MdAccountCircle, MdEdit } from "react-icons/md";

export function useAccountMenu() {
    const [ isOpen, setIsOpen ] = useState( false );
    const menuRef = useRef( null ) as RefObject<HTMLDivElement>;
    const toggleAccountMenu = () => setIsOpen( !isOpen );

    const menuItems = [
        { icon: <MdAccountCircle className="account-menu__item-icon" />, title: 'Мій профіль', href: '/profile' },
        { icon: <MdEdit className="account-menu__item-icon" />, title: 'Редагувати профіль', href: '/profile/edit' },
    ];

    useEffect( () => {
        const handleClickOutside = ( event ) => {
            if ( menuRef.current && !menuRef.current.contains( event?.target ) ) {
                toggleAccountMenu();
            }
        };

        document.addEventListener( 'click', handleClickOutside );
        return () => {
            document.removeEventListener( 'click', handleClickOutside );
        };
    }, [ menuRef, isOpen ] );

    return {
        isOpen, toggleAccountMenu, menuItems, menuRef
    }
}
