export function useNavigation() {
    const navItems: {
        href: string
        label: string
        className?: string
    }[] = [
        { href: '/about', label: 'Про платформу' },
        { href: '/technologies', label: 'Наші технології' },
        { href: '/users', label: 'Анкети', className: 'btn-primary' },
    ];

    return navItems
}
