import Link from 'next/link';
import Image from "next/image";
import { useNavigation, useHeader } from "@/widgets/header/model";
import { NavigationLayout } from "@/widgets/header/ui/navigation-layout";
import { NavigationItem } from "@/widgets/header/ui/navigation-item";

export const Header = () => {
    const { pathname } = useHeader();
    const navItems = useNavigation()

    return (
        <header className="header">
            <Link href={'/'}>
                <div className="header__logo">
                    <Image
                        src="/competence.png"
                        width={40}
                        height={40}
                        alt="Logo"
                        className = "header__logo-image"
                    />
                    <span className="header__title">My skills</span>
                </div>
            </Link>

            <NavigationLayout>
                {
                    navItems.map( ( item, index ) => (
                        <NavigationItem key={`nav_item_${index}`} href={item.href} className={item.className} pathname={pathname} label={item.label}></NavigationItem>
                    ) )
                }
            </NavigationLayout>
        </header>
    );
};
