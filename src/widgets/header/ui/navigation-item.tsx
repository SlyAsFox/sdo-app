import { FC } from "react";
import Link from "next/link";

interface NavigationItemProps {
    href: string,
    className?: string | undefined,
    pathname: string,
    label: string,
}

export const NavigationItem: FC<NavigationItemProps> = ( { href, className, pathname, label } ) => {
    return (
        <Link
            key={href}
            className={ `${className || `${pathname === href ? 'underline' : ''}`}` }
            href={href} passHref
        >
            {label}
        </Link>
    );
};
