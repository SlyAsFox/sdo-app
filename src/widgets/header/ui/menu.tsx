import { FC, ReactNode } from "react";

interface MenuProps {
    children: ReactNode;
    title: string,
    href: string,
    ref: string,
    isOpen: boolean,
}

export const Menu: FC<MenuProps> = ( { ref, children, isOpen } ) => {
    return isOpen
        ? (
            <div ref={ref} className="account-menu__menu">
                <ul className="space-y-3">
                    {children}
                </ul>
            </div>
        )
        : '';
};
