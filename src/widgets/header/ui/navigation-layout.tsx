import { ReactNode, Children, Suspense } from "react";
import { GiHamburgerMenu } from "react-icons/gi";
import { useHeader } from "@/widgets/header/model";
import { AccountMenu } from "@/widgets/header/ui/account-menu";


export function NavigationLayout( { children }: { children: ReactNode } ) {
    const { user, handleLogout, isMenuOpen, toggleMenu, navRef } = useHeader()
    return (
        <nav className="header__nav">
            <div onClick={toggleMenu}>
                <GiHamburgerMenu className="header__nav__menu-icon"/>
            </div>
            {
                isMenuOpen
                    ? <div ref={navRef} className="header__nav__menu">
                        <ul className="space-y-3">
                            {
                                Children.map( children, ( child, index ) => (
                                    <li key={`nav-menu-item${index}`} className="header__nav-item--menu header__nav-item" onClick={toggleMenu}>
                                        {child}
                                    </li>
                                ) )
                            }
                        </ul>
                    </div>
                    : ''
            }
            <div className="header__nav__bar">
                {
                    Children.map( children, ( child, index ) => (
                        <div key={`nav-bar-item${index}`} className="header__nav-item header__nav-item--bar">
                            {child}
                        </div>
                    ) )
                }
            </div>
            <div className="header__divider"></div>
            <Suspense fallback={<p>Loading feed...</p>}>
                <AccountMenu user={user} onLogout={handleLogout}/>
            </Suspense>
        </nav>
    )
}
