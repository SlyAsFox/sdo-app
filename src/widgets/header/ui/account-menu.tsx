"use client"
import Link from 'next/link';
import { MdLogout } from "react-icons/md";
import { avatarUrl } from "@/shared/datasets";
import { MenuItem } from "@/widgets/header/ui/menu-item";
import { useAccountMenu } from "@/widgets/header/model";


export const AccountMenu = ( { user, onLogout } ) => {
    const { toggleAccountMenu, isOpen, menuItems, menuRef } = useAccountMenu()

    return user
        ? (
            <div
                className="account-menu"
            >
                <div
                    onClick={toggleAccountMenu}
                    className={`account-menu__toggle`}
                >
                    <div className="account-menu__user">
                        <div className="account-menu__avatar">
                            <img src={user.avatarPreview || avatarUrl} alt={user.name} className="account-menu__avatar" />
                        </div>
                        <div className="account-menu__username">
                            <div>{user.name}</div>
                        </div>
                    </div>
                </div>
                {isOpen && (
                    <div ref={menuRef} className="account-menu__menu">
                        <ul className="space-y-3">
                            {
                                menuItems.map( ( item ) => (
                                    <MenuItem href={item.href} title={item.title} icon={item.icon} key={item.href}/>
                                ) )
                            }
                            <hr className="account-menu__divider"/>
                            <li className="account-menu__item">
                                <a onClick={onLogout} className="account-menu__item-link-logout">
                                    <MdLogout className="account-menu__item-icon" size={24}/>
                                    Вийти
                                </a>
                            </li>
                        </ul>
                    </div>
                )}
            </div>
        )
        : (
            <Link className="header__nav-link" href="/login" passHref>
                Увійти
            </Link>
        )
    ;
};
