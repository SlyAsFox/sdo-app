import Link from "next/link";
import { FC, ReactNode } from "react";
import { useAccountMenu } from "@/widgets/header/model/account-menu.model";

interface MenuItemProps {
    title: string,
    href: string,
    icon: ReactNode,
    key: string,
}

export const MenuItem: FC<MenuItemProps> = ( { title, href, icon, key } ) => {
    const { toggleAccountMenu } = useAccountMenu()

    return (
        <li key={key} className="account-menu__item" onClick={toggleAccountMenu}>
            <Link href={href} className="account-menu__item-link">
                {icon}
                {title}
            </Link>
        </li>
    );
};
