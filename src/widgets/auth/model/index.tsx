"use client"
import { useRef, useState } from "react";
import { enqueueSnackbar } from "notistack";
import { SnackbarTypesEnum } from "@/shared/datasets";
import api from "@/shared/api";
import { useRouter } from "next/navigation";

export function useAuth() {
    const [ signIn, setSignIn ] = useState( true );

    const loginEmailRef = useRef<HTMLInputElement>( null );
    const loginPasswordRef = useRef<HTMLInputElement>( null );
    const signupNameRef = useRef<HTMLInputElement>( null );
    const signupEmailRef = useRef<HTMLInputElement>( null );
    const signupPasswordRef = useRef<HTMLInputElement>( null );
    const router = useRouter();

    const toggle = () => {
        setSignIn( !signIn );
    }

    const handleLogin = async () => {
        const email = loginEmailRef.current?.value;
        const password = loginPasswordRef.current?.value;

        if ( !email || !password ) {
            enqueueSnackbar( 'Email and password are required', { variant: SnackbarTypesEnum.ERROR } );
            return;
        }

        try {
            await api.login( email, password );
            router.push( '/' );
        } catch ( error ) {
            enqueueSnackbar( error.message, { variant: SnackbarTypesEnum.ERROR } );
        }
    };

    const handleSignup = async () => {
        const name = signupNameRef.current?.value;
        const email = signupEmailRef.current?.value;
        const password = signupPasswordRef.current?.value;

        if ( !name || !email || !password ) {
            enqueueSnackbar( 'Name, email, and password are required', { variant: SnackbarTypesEnum.ERROR } );
            return;
        }

        try {
            await api.signup( { name, email, password } );
            router.push( '/profile/edit' );

        } catch ( error ) {
            enqueueSnackbar( error.message, { variant: SnackbarTypesEnum.ERROR } );
        }
    };

    return {
        signIn,
        toggle,
        loginPasswordRef,
        loginEmailRef,
        signupNameRef,
        signupEmailRef,
        signupPasswordRef,
        handleLogin,
        handleSignup
    }
}

