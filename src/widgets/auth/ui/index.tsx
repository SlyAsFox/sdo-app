"use client"
import { FC } from 'react';
import Link from "next/link";
import { Layout } from "@/widgets/auth/ui/layout";
import { SignUpLayout } from "@/widgets/auth/ui/sign-up-layout";
import { FormLayout } from "@/widgets/auth/ui/form-layout";
import { Title } from "@/widgets/auth/ui/title";
import { Input } from "@/widgets/auth/ui/input";
import { Button } from "@/widgets/auth/ui/button";
import { FormFooter } from "@/widgets/auth/ui/form-footer";
import { SignInLayout } from "@/widgets/auth/ui/sign-in-layout";
import { OverlayContainer } from "@/widgets/auth/ui/overlay-container";
import { Overlay } from "@/widgets/auth/ui/overlay";
import { OverlayPanel } from "@/widgets/auth/ui/overlay-panel";
import { Paragraph } from "@/widgets/auth/ui/paragraph";
import { useAuth } from "@/widgets/auth/model";


export const AuthForm: FC = () => {
    const { signIn, toggle, loginPasswordRef, loginEmailRef, signupNameRef, signupEmailRef, signupPasswordRef, handleLogin, handleSignup } = useAuth()

    return (
        <Layout>
            <SignUpLayout signIn={signIn}>
                <FormLayout>
                    <Title className="signup-page__title">Створити обліковий запис</Title>
                    <Input type='text' placeholder='Імя' ref={signupNameRef} />
                    <Input type='email' placeholder='Email' ref={signupEmailRef} />
                    <Input type='password' placeholder='Пароль' ref={signupPasswordRef} />
                    <Button onClick={handleSignup}>Зареєструватися</Button>
                    <FormFooter>
                        Вже є акаунт? <Link href="#" className="login-form__link" onClick={toggle}>Увійти</Link>
                    </FormFooter>
                </FormLayout>
            </SignUpLayout>

            <SignInLayout signIn={signIn}>
                <FormLayout>
                    <Title>Вхід</Title>
                    <Input type='email' placeholder='Email' ref={loginEmailRef}/>
                    <Input type='password' placeholder='Пароль' ref={loginPasswordRef}/>
                    {/*<Anchor href='#'>Забули пароль?</Anchor>*/}
                    <Button onClick={handleLogin}>Увійти</Button>
                    <FormFooter>
                        Ще немає акаунту? <Link href="#" className="login-form__link" onClick={toggle}>Зареєструватись</Link>
                    </FormFooter>
                </FormLayout>
            </SignInLayout>

            <OverlayContainer signIn={signIn}>
                <Overlay signIn={signIn}>
                    <OverlayPanel position={`signup-page__overlay-panel--left ${!signIn ? 'signup-page__overlay-panel--left--signup' : ''}`}>
                        <Title>Ласкаво просимо назад!</Title>
                        <Paragraph className={'signup-page__paragraph'}>Щоб продовжити просувати свої професійні навички, увійдіть за допомогою своїх облікових даних.</Paragraph>
                        <Button onClick={toggle}>Увійти</Button>
                    </OverlayPanel>

                    <OverlayPanel position={`signup-page__overlay-panel--right ${!signIn ? 'signup-page__overlay-panel--right--signup' : ''}`}>
                        <Title>Привіт, Друже!</Title>
                        <Paragraph className={'signup-page__paragraph'}>Створіть обліковий запис і почніть просувати свої професійні навички разом з нами.</Paragraph>
                        <Button onClick={toggle}>Зареєструватися</Button>
                    </OverlayPanel>
                </Overlay>
            </OverlayContainer>
        </Layout>
    );
};
