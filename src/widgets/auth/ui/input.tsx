import { forwardRef, InputHTMLAttributes, ForwardedRef } from 'react';

const Input = forwardRef<HTMLInputElement, InputHTMLAttributes<HTMLInputElement>>( ( props, ref: ForwardedRef<HTMLInputElement> ) => (
    <input className="signup-page__input" ref={ref} {...props} />
) );

Input.displayName = 'Input';

export { Input };
