import { FC, ReactNode } from "react";

interface FormLayoutProps {
    children: ReactNode;
}

export const FormLayout: FC<FormLayoutProps> = ( { children } ) => {
    return <div className="signup-page__form">{children}</div>;
};
