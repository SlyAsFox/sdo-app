import { FC, ReactNode } from "react";

interface OverlayProps {
    children: ReactNode;
    signIn: boolean;
}

export const Overlay: FC<OverlayProps> = ( { children, signIn } ) => {
    return <div className={`signup-page__overlay ${!signIn ? 'signup-page__overlay--signup' : ''}`}>{children}</div>;
};
