import { FC, ReactNode } from "react";

interface TitleProps {
    children: ReactNode;
}

export const Title: FC<TitleProps> = ( { children } ) => (
    <h1 className="signup-page__title">
        {children}
    </h1>
);
