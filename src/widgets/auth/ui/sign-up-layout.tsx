import { FC, ReactNode } from "react";

interface SignUpLayoutProps {
    children: ReactNode;
    signIn: boolean;
}

export const SignUpLayout: FC<SignUpLayoutProps> = ( { children, signIn } ) => {
    return <div className={`signup-page__sign-up-layout ${!signIn ? 'signup-page__sign-up-layout--signup' : ''}`}>
        {children}
    </div>;
};
