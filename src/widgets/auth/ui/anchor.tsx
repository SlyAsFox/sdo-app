import { FC, ReactNode } from "react";
import Link from "next/link";

interface AnchorProps {
    children: ReactNode;
    href: string;
}

export const Anchor: FC<AnchorProps> = ( { children, href } ) => (
    <Link className="signup-page__anchor" href={href}>
        {children}
    </Link>
);

