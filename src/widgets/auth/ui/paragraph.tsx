import { FC, ReactNode } from "react";

interface ParagraphProps {
    children: ReactNode;
}

export const Paragraph: FC<ParagraphProps> = ( { children } ) => (
    <p className="signup-page__paragraph">
        {children}
    </p>
);
