import { FC, ReactNode } from "react";

interface OverlayPanelProps {
    children: ReactNode;
    position: string;
}

export const OverlayPanel: FC<OverlayPanelProps> = ( { children, position } ) => (
    <div className={`signup-page__overlay-panel ${position}`}>
        {children}
    </div>
);
