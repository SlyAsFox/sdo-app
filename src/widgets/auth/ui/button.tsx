import { FC, ReactNode, MouseEvent } from "react";

interface ButtonProps {
    children: ReactNode;
    onClick: ( event: MouseEvent<HTMLButtonElement> ) => void;
}

export const Button: FC<ButtonProps> = ( { children, onClick } ) => (
    <button
        className={`signup-page__button`}
        onClick={onClick}
    >
        {children}
    </button>
);

