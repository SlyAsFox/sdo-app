import { FC, ReactNode } from "react";

interface OverlayContainerProps {
    children: ReactNode;
    signIn: boolean;
}

export const OverlayContainer: FC<OverlayContainerProps> = ( { children, signIn } ) => {
    return <div className={`signup-page__overlay-container ${!signIn ? 'signup-page__overlay-container--signup' : ''}`}>{children}</div>;
};
