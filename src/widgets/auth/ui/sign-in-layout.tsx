import { FC, ReactNode } from "react";

interface SignInLayoutProps {
    children: ReactNode;
    signIn: boolean;
}

export const SignInLayout: FC<SignInLayoutProps> = ( { children, signIn } ) => {
    return <div className={`signup-page__sign-in-layout ${!signIn ? 'signup-page__sign-in-layout--signup' : ''}`}>
        {children}
    </div>;
};
