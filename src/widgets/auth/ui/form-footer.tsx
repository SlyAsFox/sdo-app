import { FC, ReactNode } from "react";

interface FormFooterProps {
    children: ReactNode;
}

export const FormFooter: FC<FormFooterProps> = ( { children } ) => (
    <p className="form-footer">
        {children}
    </p>
);
