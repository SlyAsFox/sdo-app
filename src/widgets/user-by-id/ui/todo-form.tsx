import { ChangeEvent, FC, FormEvent } from 'react';

interface TodoListProps {
    onSubmit: ( event: FormEvent<HTMLFormElement> ) => void;
    inputOnChange: ( e: ChangeEvent<HTMLInputElement> ) => void;
    value: string
}

export const TodoForm: FC<TodoListProps> = ( { onSubmit, inputOnChange, value } ) => {

    return (
        <form className="todo-list__form" onSubmit={onSubmit}>
            <input
                type="text"
                className="todo-list__form__input"
                placeholder="Що хочете вивчити?"
                value={value}
                onChange={inputOnChange}
            />
            <button
                type="submit"
                className="todo-list__form__button"
            >
                Додати
            </button>
        </form>
    );
};
