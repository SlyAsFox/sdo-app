import { Loader } from "@/shared/ui";
import ErrorFallback from "@/shared/ui/ErrorFallback";
import ErrorBoundary from "@/shared/ui/ErrorBoundary";
import { FC } from "react";
import { useUserProfile } from "@/widgets/user-by-id/model";
import UserProfile from "@/widgets/user-by-id/ui/user-profile";
import { TodoList } from "@/widgets/user-by-id/ui/todo-list";

interface UserByIdProps {
    id: string;
}

export const UserById: FC<UserByIdProps> = ( { id } ) => {
    const { user, loading, error } = useUserProfile( id );

    if ( loading ) {
        return <Loader />;
    }

    if ( error ) {
        return (
            <ErrorFallback onRetry={() => window.location.reload()} />
        );
    }

    if ( !user ) {
        return <ErrorFallback onRetry={() => window.location.reload()} message="User not found" />;
    }

    return (
        <section className="user-by-id-page">
            <div className={'user-by-id-page__container'}>
                <UserProfile {...user} />
                <ErrorBoundary fallback={<ErrorFallback onRetry={() => window.location.reload()} />}>
                    <TodoList userId={user.id}/>
                </ErrorBoundary>
            </div>
        </section>
    );
}
