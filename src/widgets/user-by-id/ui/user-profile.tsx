import { FC } from 'react';
import { PiBagSimpleFill } from "react-icons/pi";
import { AiOutlineGlobal } from "react-icons/ai";
import { FaFacebook, FaGithub, FaInstagram, FaLinkedin } from "react-icons/fa";
import Link from "next/link";
import { avatarUrl } from "@/shared/datasets";

interface UserProfileProps {
    name: string;
    title?: string;
    description?: string;
    avatarOriginal?: string;
    githubUrl?: string;
    instagramUrl?: string;
    linkedinUrl?: string;
    facebookUrl?: string;
    location?: string,
}

const UserProfile: FC<UserProfileProps> = ( { name, title, description, avatarOriginal, githubUrl, instagramUrl, linkedinUrl, facebookUrl, location } ) => {
    return (
        <div className="user-profile">
            <div className="user-profile__details">
                <h1 className="user-profile__title">{name}</h1>

                <div className="user-profile__divider"></div>
                {
                    title && (
                        <div className="user-profile__item">
                            <PiBagSimpleFill size={20} className={'user-profile__item__title-icon'}/>
                            <span className={'user-profile__item__title-text'}>{title}</span>
                        </div>
                    )
                }
                {
                    location && (
                        <div className="user-profile__item mb-4">
                            <AiOutlineGlobal size={20} className={'user-profile__item__location-icon'}/>
                            <p className="user-profile__item__location-text"><span >{`Місцезнаходження — `}</span> {`${location}`}</p>
                        </div>
                    )
                }
                {
                    description && (
                        <p className="user-profile__description">{description}</p>
                    )
                }
            </div>
            <div className="user-profile__avatar">
                <img src={avatarOriginal || avatarUrl} alt={name} className="user-profile__avatar__image" />
                <div className="user-card__social-icons">
                    {
                        githubUrl && (
                                <Link href={githubUrl} target="_blank" rel="noopener noreferrer">
                                    <FaGithub />
                                </Link>
                            )
                    }
                    {
                        instagramUrl && (
                                <Link href={instagramUrl} target="_blank" rel="noopener noreferrer">
                                    <FaInstagram />
                                </Link>
                            )
                    }
                    {
                        linkedinUrl
                            ? (
                                <Link href={linkedinUrl} target="_blank" rel="noopener noreferrer">
                                    <FaLinkedin />
                                </Link>
                            )
                            : ''
                    }
                    {
                        facebookUrl
                            ? (
                                <Link href={facebookUrl} target="_blank" rel="noopener noreferrer">
                                    <FaFacebook />
                                </Link>
                            )
                            : ''
                    }
                </div>
            </div>
        </div>
    );
};

export default UserProfile;
