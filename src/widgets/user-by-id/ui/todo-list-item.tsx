import { FC } from 'react';
import { CiSquareRemove } from "react-icons/ci";

interface TodoListProps {
    key: string;
    text: string;
    isCompleted: boolean;
    allowDelete: boolean
    onChange: () => void;
    onDelete: () => void
}

export const TodoListItem: FC<TodoListProps> = ( { key, isCompleted, onChange, text, allowDelete, onDelete  } ) => {
    return (
        <li key={key} className="todo-list__item">
            <div className="todo-list__item__col">
                <input
                    type="checkbox"
                    checked={isCompleted}
                    onChange={onChange}
                    className="todo-list__item-checkbox"
                />
                <p className={`todo-list__item-text ${isCompleted ? 'todo-list__item-text--completed' : ''}`}>{text}</p>
            </div>
            <div className="todo-list__item__col">
                {
                    !isCompleted
                        ? (
                            <p className="todo-list__item-uncompleted">{'Вивчаю'}</p>
                        )
                        : ''
                }
                {
                    allowDelete
                        ? (
                            <CiSquareRemove size={24} onClick={onDelete} className="todo-list__button-delete"/>
                        )
                        : ''
                }
            </div>
        </li>
    );
};
