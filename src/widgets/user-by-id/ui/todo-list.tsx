import { FC } from 'react';
import { Loader } from "@/shared/ui";
import ErrorFallback from "@/shared/ui/ErrorFallback";
import { TodoForm } from "@/widgets/user-by-id/ui/todo-form";
import { TodoListItem } from "@/widgets/user-by-id/ui/todo-list-item";
import { useTodoList } from "@/widgets/user-by-id";

interface TodoListProps {
    userId?: string
}

export const TodoList: FC<TodoListProps> = ( { userId } ) => {
    const { todos, loading, error, newTodo, setNewTodo, isListOwner, handleAddTodo, handleToggleComplete, handleDeleteTodo } = useTodoList( userId );

    if ( loading ) {
        return <Loader />;
    }

    if ( error ) {
        return (
            <ErrorFallback onRetry={() => window.location.reload()} message={ `Error in todos list: ${error}` } />
        );
    }

    return (
        <div className="todo-list">
            {
                todos.length || isListOwner
                    ? <h2 className="todo-list__header">{'Мій todo-лист на цей місяць'}</h2>
                    : ''
            }
            {
                isListOwner
                    ? <TodoForm value={newTodo} inputOnChange={( e ) => setNewTodo( e.target.value )} onSubmit={handleAddTodo}/>
                    : ''
            }

            <ul>
                {todos.map( todo => (
                    <TodoListItem
                        key={todo.id}
                        id={todo.id}
                        isCompleted={todo.isCompleted}
                        text={todo.text}
                        allowDelete={isListOwner}
                        onChange={() => handleToggleComplete( todo.id )}
                        onDelete={() => handleDeleteTodo( todo.id )}
                    />
                ) )}
            </ul>
        </div>
    );
};
