import { useEffect, useState } from "react";
import { UserEntity } from "@/entities/user";
import api from "@/shared/api";

export function useUserProfile( id: string ) {
    const [ user, setUser ] = useState<UserEntity>( null );
    const [ loading, setLoading ] = useState( true );
    const [ error, setError ] = useState( null );

    useEffect( () => {
        const fetchUser = async () => {
            try {
                const userData = await api.getUserById( id );
                setUser( userData as UserEntity );
            } catch ( error ) {
                setError( error );
            } finally {
                setLoading( false );
            }
        };

        fetchUser();
    }, [] );

    return {
        user, loading, error
    }
}
