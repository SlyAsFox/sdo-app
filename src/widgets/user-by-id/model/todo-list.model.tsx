import { FormEvent, useEffect, useState } from "react";
import api from "@/shared/api";
import { TodoEntity } from "@/entities/todo/todo.type";

export function useTodoList( userId ) {
    const [ todos, setTodos ] = useState<TodoEntity[]>( [] );
    const [ newTodo, setNewTodo ] = useState( '' );
    const [ error, setError ] = useState( null );
    const [ loading, setLoading ] = useState( true );
    const isListOwner = !!( localStorage.getItem( 'user' ) && JSON.parse( localStorage.getItem( 'user' ) ).id === userId );

    useEffect( () => {
        const fetchTodos = async () => {
            try {
                const todosList = await api.getUserTodos( userId );
                setTodos( todosList as TodoEntity[] );
            } catch ( error ) {
                setError( error );
            } finally {
                setLoading( false );
            }
        };

        fetchTodos();
    }, [] );



    const handleAddTodo = async ( e: FormEvent ) => {
        e.preventDefault();
        if ( newTodo.trim() !== '' ) {
            const createdTodo = await api.addTodo( { text: newTodo, isCompleted: false } ) as TodoEntity;
            setTodos( [ ...todos, createdTodo ] );
            setNewTodo( '' );
        }
    };

    const handleToggleComplete = async ( id: string ) => {
        if( isListOwner ) {
            const updatedTodo = await api.updateTodo( id, { isCompleted: !todos.find( todo => todo.id === id ).isCompleted } ) as TodoEntity;
            setTodos( todos.map( todo => ( todo.id === id ? { ...todo, ...updatedTodo } : todo ) ) );
        }
    };

    const handleDeleteTodo = async ( id: string ) => {
        if( isListOwner ) {
            await api.deleteTodo( id );
            setTodos( todos.filter( todo => todo.id !== id ) );
        }
    };

    return {
        todos, loading, error, newTodo, setNewTodo, setTodos, isListOwner, handleAddTodo, handleToggleComplete, handleDeleteTodo
    }
}
