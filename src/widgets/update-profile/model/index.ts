'use client'
import { ChangeEvent, FormEvent, useState } from "react";
import { UserEntity } from "@/entities/user";
import api from "@/shared/api";
import { SnackbarTypesEnum } from "@/shared/datasets";
import { useSnackbar } from 'notistack';
import { useRouter } from "next/navigation";
import { useQuery } from '@tanstack/react-query';

export function useEditProfile() {
    const [ formData, setFormData ] = useState<UserEntity>( {} as UserEntity );
    const [ errors, setErrors ] = useState<Record<string, string>>( {} );
    const { enqueueSnackbar } = useSnackbar();
    const router = useRouter();

    const fetchProfile = async () => {
        const data = await api.getProfile()
        setFormData( data )
        return data;
    }
    const { isLoading } = useQuery(
        {
            queryKey: [ 'profile' ],
            queryFn: fetchProfile,
            refetchOnWindowFocus: false,
            refetchInterval: false,
        }
    );

    const handleInputChange = ( e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement> ) => {
        const { name, value } = e.target;
        setFormData( { ...formData, [name]: value } );
    };

    const handleAvatarChange = ( avatarPreview: string, avatarOriginal: string ) => {
        setFormData( { ...formData, avatarPreview, avatarOriginal } );
    };

    const setFieldError = ( name: string, error: string ) => {
        setErrors( ( prevErrors ) => {
            const updatedErrors = {
                ...prevErrors,
                [name]: error,
            };
            if ( !updatedErrors[name] ) delete updatedErrors[name];
            return updatedErrors;
        } );
    };

    const validateField = ( name: string, value: string ) => {
        switch ( name ) {
            case 'name':
                if ( !value ) {
                    return 'Name is required';
                }
                break;
            case 'email':
                if ( !value ) {
                    return 'Email is required';
                } else if ( !/\S+@\S+\.\S+/.test( value ) ) {
                    return 'Email is invalid';
                }
                break;
            case 'password':
                if ( !value ) {
                    return 'Password is required';
                } else if ( value.length < 6 ) {
                    return 'Password must be at least 6 characters long';
                }
                break;
            case 'repeatPassword':
                if ( !value ) {
                    return 'Repeat password is required';
                } else if ( value !== formData.password ) {
                    return 'Passwords do not match';
                }
                break;
            default:
                return '';
        }
        return '';
    };

    const handleSubmit = async ( e: FormEvent ) => {
        e.preventDefault();
        try {
            const profileData = await api.updateProfile( formData );
            setFormData( profileData as UserEntity );
            enqueueSnackbar( 'Profile successfully updated.', { variant: SnackbarTypesEnum.SUCCESS } );
            router.push( '/profile' );
        } catch ( error ) {
            console.error( 'Failed to update profile:', error );
        }
    };

    const isFormValid = () => {
        if ( !errors ) {
            return false;
        }
        return Object.values( errors ).every( ( error ) => error === '' );
    };

    return {
        formData, errors, handleAvatarChange, handleInputChange, isFormValid, handleSubmit, validateField, setFieldError, setFormData, isLoading
    };
}
