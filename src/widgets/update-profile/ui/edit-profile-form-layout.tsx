import { ReactNode, FormEvent, FC } from "react";

interface EditProfileFormLayoutProps {
    children: ReactNode;
    handleSubmit: ( event: FormEvent<HTMLFormElement> ) => void;
}

export const EditProfileFormLayout: FC<EditProfileFormLayoutProps> = ( { children, handleSubmit } ) => {
    return (
        <div className="edit-profile-form">
            <form className="edit-profile-form__form" onSubmit={handleSubmit}>
                {children}
            </form>
        </div>
    );
}
