"use client"
import { FC } from 'react';
import { Loader, UploadPhoto } from "@/shared/ui";
import { Input } from "@/shared/ui/input/ui/Input";
import { EditProfileFormLayout } from "@/widgets/update-profile/ui/edit-profile-form-layout";
import { useEditProfile } from "@/widgets/update-profile";

export const EditProfile: FC = () => {
    const { formData, handleSubmit, handleAvatarChange, handleInputChange, validateField, isFormValid, setFieldError, isLoading } = useEditProfile<Record<string, string>>();

    if ( isLoading || !formData ) return <Loader />;

    return (
        <EditProfileFormLayout handleSubmit={handleSubmit}>
            <h2 className="edit-profile-form__title">Редагування профілю</h2>

            <UploadPhoto
                originalUrl={formData.avatarOriginal}
                previewUrl={formData.avatarPreview}
                handleAvatarChange={handleAvatarChange}
            />

            <div className="space-y-4">
                <div className={"inputs-group"}>
                    <Input
                        label="Ім'я"
                        name="name"
                        type="text"
                        value={formData?.name}
                        onChange={handleInputChange}
                        validate={validateField}
                        setErrors={setFieldError}
                    />

                    <Input
                        label="Посада"
                        name="title"
                        type="text"
                        value={formData?.title}
                        onChange={handleInputChange}
                    />

                    <Input
                        label="Email"
                        name="email"
                        type="email"
                        value={formData?.email}
                        onChange={handleInputChange}
                        validate={validateField}
                        setErrors={setFieldError}
                    />

                    <Input
                        label="Країна, місто"
                        name="location"
                        type="text"
                        value={formData?.location}
                        onChange={handleInputChange}
                    />
                </div>

                <Input
                    label="Про себе"
                    name="description"
                    type="textarea"
                    value={formData?.description}
                    onChange={handleInputChange}
                />

                <hr className={'edit-profile__hr'} />

                <div className={"inputs-group"}>
                    <Input
                        label="GitHub URL"
                        name="githubUrl"
                        type="url"
                        value={formData?.githubUrl}
                        onChange={handleInputChange}
                    />

                    <Input
                        label="LinkedIn URL"
                        name="linkedinUrl"
                        type="url"
                        value={formData?.linkedinUrl}
                        onChange={handleInputChange}
                    />

                    <Input
                        label="Instagram URL"
                        name="instagramUrl"
                        type="url"
                        value={formData?.instagramUrl}
                        onChange={handleInputChange}
                    />

                    <Input
                        label="Facebook URL"
                        name="facebookUrl"
                        type="url"
                        value={formData?.facebookUrl}
                        onChange={handleInputChange}
                    />
                </div>
            </div>

            <div className="edit-profile-form__button-wrapper">
                <button
                    type="submit"
                    className="btn-primary"
                    disabled={!isFormValid()}
                >
                    Зберегти зміни
                </button>
            </div>
        </EditProfileFormLayout>
    );
};
