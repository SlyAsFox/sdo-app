"use client";
import { useEffect } from "react";
import { useSnackbar } from "notistack";
import { useInfiniteQuery, QueryFunctionContext } from '@tanstack/react-query';
import { useRouter } from "next/navigation";
import api from "@/shared/api";
import { UserEntity } from "@/entities/user";
import { SnackbarTypesEnum } from "@/shared/datasets";

type FetchUsersResponse = {
    usersList: UserEntity[];
    nextOffset: number;
};

const fetchUsers = async ( { pageParam = 0 }: QueryFunctionContext<[string]> ): Promise<FetchUsersResponse> => {
    const usersPerPage = 3;
    const { data: usersList } = await api.getUsers( {
        limit: usersPerPage,
        offset: pageParam,
    } );
    return { usersList, nextOffset: pageParam + usersPerPage };
};

export default function useUsersList() {
    // @ts-ignore
    const {
        data,
        error,
        fetchNextPage,
        hasNextPage,
        isFetchingNextPage,
        status,
    } = useInfiniteQuery<FetchUsersResponse, Error>(
        {
            queryKey: [ 'users' ],
            queryFn: fetchUsers,
            getNextPageParam: ( lastPage ) => lastPage.usersList.length === 0 ? undefined : lastPage.nextOffset,
        }
    );
    const { enqueueSnackbar } = useSnackbar();
    const router = useRouter();

    useEffect( () => {
        const handleScroll = () => {
            if ( window.innerHeight + window.scrollY >= document.body.offsetHeight - 2 && hasNextPage && !isFetchingNextPage ) {
                fetchNextPage();
            }
        };

        window.addEventListener( "scroll", handleScroll );
        return () => window.removeEventListener( "scroll", handleScroll );
    }, [ fetchNextPage, hasNextPage, isFetchingNextPage ] );

    useEffect( () => {
        const storedUser = localStorage.getItem( 'user' );
        if ( !storedUser ) {
            router.push( '/login' );
        }
    }, [] );

    useEffect( () => {
        if ( error ) {
            enqueueSnackbar( error, { variant: SnackbarTypesEnum.ERROR } );
        }
    }, [ error, enqueueSnackbar ] );

    return {
        users: data?.pages.flatMap( page => page.usersList ) || [],
        loading: status === 'loading' || isFetchingNextPage,
        error,
    };
}
