import { FC } from 'react';
import Link from "next/link";
import { FaGithub, FaInstagram, FaLinkedin, FaFacebook } from 'react-icons/fa';
import { PiBagSimpleFill } from "react-icons/pi";
import { AiOutlineGlobal } from "react-icons/ai";
import { avatarUrl } from "@/shared/datasets";

interface UserCardProps {
    id: string;
    name: string;
    title: string;
    location: string;
    description: string;
    avatar: string;
    githubUrl: string;
    instagramUrl: string;
    linkedinUrl: string;
    facebookUrl: string;
}

export const UserCard: FC<UserCardProps> = (
    {
        id,
        name,
        title,
        location,
        description,
        avatar,
        githubUrl,
        instagramUrl,
        linkedinUrl,
        facebookUrl
    } ) => {
        return (
            <div className="user-card">
                <div className="user-card__content">
                    <h2 className="user-card__name">{name}</h2>
                    <hr className="user-card__divider"/>
                    <div className="user-card__title">
                        <PiBagSimpleFill className="user-card__title-icon" size={20}/>
                        <span className="font-medium">{title}</span>
                    </div>
                    <div className="user-card__location">
                        <AiOutlineGlobal className="user-card__location-icon" size={20}/>
                        <span className="font-medium">{location}</span>
                    </div>
                    <p className="user-card__description">{description}</p>
                    <div className="user-card__bottom">
                        <Link href={`/users/${id}`}>
                            <button className="user-card__button">
                                Дивитись анкету
                            </button>
                        </Link>
                        <div className="user-card__social-icons">
                            {
                                githubUrl
                                    ? <Link href={githubUrl || ''} target="_blank" rel="noopener noreferrer">
                                        <FaGithub />
                                    </Link>
                                    : <FaGithub />
                            }
                            {
                                instagramUrl
                                    ? <Link href={instagramUrl || ''} target="_blank" rel="noopener noreferrer">
                                        <FaInstagram />
                                    </Link>
                                    : <FaInstagram />
                            }
                            {
                                linkedinUrl
                                    ? <Link href={linkedinUrl || ''} target="_blank" rel="noopener noreferrer">
                                        <FaLinkedin />
                                    </Link>
                                    : <FaLinkedin />
                            }
                            {
                                facebookUrl
                                    ? <Link href={facebookUrl || ''} target="_blank" rel="noopener noreferrer">
                                        <FaFacebook />
                                    </Link>
                                    : <FaFacebook />
                            }
                        </div>
                    </div>
                </div>
                <div className="user-card__avatar">
                    <img src={avatar || avatarUrl} alt={name} className="w-full h-full object-cover" />
                </div>
            </div>
        );
    };
