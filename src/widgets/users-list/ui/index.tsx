"use client";
import { FC } from "react";
import { Loader } from "@/shared/ui";
import useUsersList from "@/widgets/users-list/model";
import { UserCard } from "@/widgets/users-list/ui/user-card";

export const UsersList: FC = () => {
    const { users, loading } = useUsersList();

    return (
        <div>
            <div className="users-page__grid">
                {users.map( ( user ) => (
                    <UserCard
                        id={user.id || ''}
                        key={`user_${user.id || ''}`}
                        name={user.name}
                        title={user.title || ''}
                        location={user.location || ''}
                        description={user.description || ''}
                        avatar={user.avatarPreview}
                        githubUrl={user.githubUrl}
                        instagramUrl={user.instagramUrl}
                        linkedinUrl={user.linkedinUrl}
                        facebookUrl={user.facebookUrl}
                    />
                ) )}
            </div>
            {loading && (
                <div className="page__loading">
                    <Loader />
                </div>
            )}
        </div>
    );
}
