import { useFeatureList } from "../model";
import { FC } from "react";
import { FeatureListLayout } from "@/widgets/features-list/ui/feature-list-layout";
import { FeatureCard } from "@/widgets/features-list/ui/feature-card";

export const FeatureList: FC = () => {
    const list = useFeatureList();

    return (
        <FeatureListLayout>
            {
                list.map( ( feature ) => (
                    <FeatureCard key={feature.id} {...feature} />
                ) )
            }
        </FeatureListLayout>
    );
}
