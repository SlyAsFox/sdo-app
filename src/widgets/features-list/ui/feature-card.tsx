import { FC, ElementType } from 'react';

export enum Align {
    LEFT = 'left',
    RIGHT = 'right',
}

interface FeatureCardProps {
    icon: ElementType;
    title: string;
    text: string;
    align: Align;
}

export const FeatureCard: FC<FeatureCardProps> = ( { icon: Icon, title, text, align } ) => {
    return (
        <div className={`feature-card ${align === Align.RIGHT ? 'feature-card--right' : 'feature-card--left'}`}>
            <Icon className={`feature-card__icon ${align === Align.RIGHT ? 'feature-card__icon--right' : 'feature-card__icon--left'}`} />
            <div className={`${align === Align.RIGHT ? 'feature-card__content--right' : 'feature-card__content--left'}`}>
                <h2 className="feature-card__title">{title}</h2>
                <p className="feature-card__text">{text}</p>
            </div>
        </div>
    );
};
