import { ReactNode } from "react";

export function FeatureListLayout( { children }: { children: ReactNode } ) {
    return <div className="about__features">{children}</div>;
}
