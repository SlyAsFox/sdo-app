import { HiMiniCheck, HiMiniUser } from "react-icons/hi2";
import { Align } from "@/widgets/features-list/ui/feature-card";

export function useFeatureList() {
    return [
        {
            id: "11111",
            icon: HiMiniCheck,
            title: "Особисті профілі",
            text: "Створіть унікальний профіль, де будуть відображатися ключові навички та робочий досвід. Це ваша цифрова візитка, яка представляє вас у кращому світлі.",
            align: Align.LEFT
        },
        {
            id: "22222",
            icon: HiMiniUser,
            title: "Власний список цілей",
            text: "Використовуйте профіль для керування своїми завданнями та цілями. Автоматично відзначайте процес виконання завдань та отримуйте відгуки.",
            align: Align.RIGHT
        },
        {
            id: "33333",
            icon: HiMiniCheck,
            title: "Особисті профілі",
            text: "Створіть унікальний профіль з ключовими навичками.",
            align: Align.LEFT
        },
        {
            id: "44444",
            icon: HiMiniUser,
            title: "Власний список цілей",
            text: "Використовуйте профіль для керування своїми завданнями та цілями. Автоматично відзначайте процес виконання завдань та отримуйте відгуки.",
            align: Align.RIGHT
        },
    ];
}

