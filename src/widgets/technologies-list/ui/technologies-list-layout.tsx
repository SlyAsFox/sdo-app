import { ReactNode } from "react";

export function TechnologiesListLayout( { children }: { children: ReactNode } ) {
    return (
        <section className="technologies-list">
            <h1 className="technologies-page__title">Наші технології</h1>
            <div className="technologies-page__grid">
                {children}
            </div>
        </section>
    );
}
