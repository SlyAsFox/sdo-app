import { TechnologiesListLayout } from "@/widgets/technologies-list/ui/technologies-list-layout";
import { TechnologyCard } from "@/widgets/technologies-list/ui/technology-card";
import { useTechnologiesList } from "@/widgets/technologies-list/model";
import { FC } from "react";

export const TechnologiesList: FC = () => {
    const technologies = useTechnologiesList();
    return (
        <TechnologiesListLayout>
            {technologies.map( ( tech, index ) => (
                <TechnologyCard
                    key={index}
                    icon={tech.icon}
                    name={tech.name}
                    version={tech.version}
                />
            ) )}
        </TechnologiesListLayout>
    );
};
