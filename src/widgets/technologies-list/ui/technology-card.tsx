import { ElementType, FC } from 'react';

interface TechnologyCardProps {
    icon: ElementType;
    name: string;
    version: string;
}

export const TechnologyCard: FC<TechnologyCardProps> = ( { icon: Icon, name, version } ) => {
    return (
        <div className="technology-card">
            <Icon size={48} className="technology-card__icon" />
            <div>
                <h2 className="technology-card__name">{name}</h2>
                <p className="technology-card__version">{version}</p>
            </div>
        </div>
    );
};
