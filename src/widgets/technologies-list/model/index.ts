import { FaNodeJs, FaReact } from "react-icons/fa";
import { SiMongodb, SiNextdotjs, SiTailwindcss, SiTypescript } from "react-icons/si";

export function useTechnologiesList() {
    return  [
        { icon: FaNodeJs, name: 'NodeJS', version: 'v18.20.0' },
        { icon: SiMongodb, name: 'MongoDB', version: 'з Mongoose' },
        { icon: SiTypescript, name: 'TypeScript', version: 'v5.3.3' },
        { icon: SiNextdotjs, name: 'Next.js', version: 'v14.0.4' },
        { icon: FaReact, name: 'React', version: 'v18.2.0' },
        { icon: SiTailwindcss, name: 'Tailwind CSS', version: 'v3.4.0' },
    ];
}

