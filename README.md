# SDO App API
> 1.0

Covent SDO App. Main functionality: login / registration for users, view other users profiles, personal todo list for each user.

## Requirements

- node `v21.2.0`


## Used in project

- Nest 14.2.3

## Getting started

Project use external API for fetch data, so project (https://gitlab.com/SlyAsFox/sdo-app-back) should be started.

1. Start the Backend Project - Ensure the backend project is running on a port other than 3000 
2. Installing dependencies:  ```npm i```
3. Run app: ```npm run dev```
